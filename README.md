# Social package for Atlashboard.

## Twitter module
You will need to set up these configuration parameters:
<pre>
"twitter" : {
    "twitterTitle" : "Latest tweets",
    "twitterSearch" : "#zentyal",
    "numberOfTweets" : "5"
}
</pre>

Also you have to set your Twitter credentials:
<pre>
{
    "twitter" : {
        "consumer_key": "",
        "consumer_secret": "",
        "access_token": "",
        "access_token_secret": ""
    }
}
</pre>
